import requests

from os import path
from io import BytesIO
from PIL import Image
from core import Point, Map, Cache


API_SERVER = "http://static-maps.yandex.ru/1.x/"
CACHE = Cache(path.abspath('./assets'), ttl=5)


def get_map(
        point: Point,
        zoom: int = 9,
        layout: str = "map"
) -> Map | Exception:
    delta = 0.002
    if zoom < 0 or zoom > 17:
        zoom = 17
    layout = layout if layout in ["map", "sat", "skl"] else "map"
    key = '_'.join([str(s) for s in [
        point.lat,
        point.lng,
        zoom,
        layout
    ]])
    filename = CACHE.get(key)
    if filename:
        print('From cache')
        return Map(
            point=point,
            link=filename,
            layout=layout,
            zoom=zoom
        )
    params = {
        "ll": f"{point.lng},{point.lat}",
        "spn": f"{delta},{delta}",
        "l": layout,
        "z": zoom
    }
    response = requests.get(API_SERVER, params=params)
    if not response:
        raise Exception(response.status_code)
    return Map(
        point=point,
        link=CACHE.set(key, BytesIO(response.content)),
        layout=layout,
        zoom=zoom
    )


try:
    lng = 37.530887
    lat = 55.703118
    zoom = 13
    point = Point(lat=lat, lng=lng)
    map = get_map(point=point, zoom=zoom, layout="sat")
    Image.open(map.link).show()
except Exception as e:
    print(f'Error: {e}')



