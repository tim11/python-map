from dataclasses import dataclass


@dataclass(frozen=True)
class Point:
    lat: float
    lng: float

    def __repr__(self):
        return f'Point({self.lat}, {self.lng})'
