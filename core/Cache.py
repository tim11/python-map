import os
import datetime
import time
from io import BytesIO


class Cache:
    def __init__(self, path: str, ttl: int = 0):
        folder = os.path.abspath(path)
        if not os.path.exists(folder) or not os.path.isdir(folder):
            os.mkdir(folder, 0o0755)
        self.folder = folder
        self.ttl = ttl


    def get(self, key: str) -> str | None:
        filename = os.path.join(
            self.folder,
            f'{key}.jpg'
        )
        if os.path.exists(filename) and os.path.isfile(filename):
            if not self.ttl:
                return filename
            cur_time = int(time.mktime(datetime.datetime.now().timetuple()))
            mod_time = int(os.path.getmtime(filename))
            if cur_time - mod_time <= self.ttl:
                return filename


    def set(self, key: str, data: BytesIO) -> str:
        filename = os.path.join(
            self.folder,
            f'{key}.jpg'
        )
        with open(filename, "wb") as f:
            f.write(data.read())
        return filename
