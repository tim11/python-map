from dataclasses import dataclass
from core.Point import Point


@dataclass(frozen=True)
class Map:
    point: Point
    link: str
    layout: str
    zoom: int

    def __repr__(self):
        output = {
            'point': str(self.point),
            'link': self.link,
            'layout': self.layout,
            'zoom': self.zoom
        }
        return f'Map({output})'
